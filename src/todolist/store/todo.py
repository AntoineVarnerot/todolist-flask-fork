from typing import List
from domain.todo import Todo
class TodoStore:
    todo_list = {}

    def add_todo(self, todo: Todo) -> Todo:
        self.todo_list[todo.id] = todo
        return todo

    def get_todo(self) -> List[Todo]:
        return list(self.todo_list.values())