import pytest
import uuid
import json
from app.app import get_app, get_store
from domain.todo import Todo
import app.todo # load http route definition

app = None

@pytest.fixture()
def app():
    app = get_app()
    # other setup can go here
    yield app
    # clean up / reset resources here

@pytest.fixture()
def store():
    store = get_store()
    yield store
    store.todo_list = {} # empty store

@pytest.fixture()
def client(app):
    print(app)
    return app.test_client()


@pytest.fixture()
def runner(app):
    return app.test_cli_runner()


def test_metrics_endpoint(client, store):
    # Given
    
    # When
    response = client.get("/metrics")

    # Then
    assert response.status_code == 200

if __name__ == '__main__':
    unittest.main()
